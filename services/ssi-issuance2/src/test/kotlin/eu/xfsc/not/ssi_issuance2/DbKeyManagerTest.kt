package eu.xfsc.not.ssi_issuance2

import eu.xfsc.not.ssi_issuance2.application.CryptoConverter
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class DbKeyManagerTest {

    @Inject
    lateinit var cryptoConv: CryptoConverter

    @Test
    fun `test Encryption`() {
        val testData = "Hełło Wörld!"
        val encData = cryptoConv.convertToDatabaseColumn(testData)
        val decData = cryptoConv.convertToEntityAttribute(encData)
        assertEquals(testData, decData)
        assertNotEquals(testData, encData)
    }

}
